# Brutal Blogging

The simplest static site generator that could possibly work.


## Install and Run

    python3 setup.py install --user

    ~/.local/bin/brutal your_blog_dir

Converts `.md` (markdown) files into HTML.

Running `brutal --gen` generates brutal metadata for the current
date and Unix username.
It can be used to start a markdown file:

    brutal --gen > newfile.md

Or in `vi` in can be used to insert the metadata into the
current document:

    :r! brutal --gen


## Markdown Metadata

Put a block like this at the top of your markdown files:

    [brutal]: #title "using Python Markdown"
    [brutal]: #author "David Jones"
    [brutal]: #date "2016-05-23"

The general form of metadata is:

    [brutal]: #metatag "blah blah blah"

Where _metatag_ can be one of `date`, `title`, `author`; or in fact anything else, but `brutal` currently only uses those three.

In order to be considered as a brutal article and converted to HTML,
a markdown file must have at least one metadata in the above form.
Even non-blog articles will probably want `title`, but if
you don't want to have `title`, `author`, nor `date`,
a placeholder metadata will work:

    [brutal]: #placeholder "yes"

The brutal metadata is valid (but obscure) markdown that
renders away to nothing.
Generally this metadata appears in a block at the beginning of the file,
but `brutal` scans the whole file for it.

The metatags that conversion to HTML recognises are:

`date`: documents with a date appear in the index, and the date appears in a `<time>` tag.

`title`: documents with a title have a `<title>` tag generated and a `<h1>` with the same text.

`author`: documents with an author have an `<address>` tag generated.

## index.html

`brutal` is intended to be used to generate a blog,
so it generates a `index.html` file that lists all
articles in date order.
Only articles with `date` metadata appear in this list.

## Plugins

There is a plugin mechanism,
that allows you to write Python to
arbitrarily change each processed article.
For example,
you can add a navigation section.

To make a plugin, make a file called `brutal_something.py`; and
in that file, define a function with a name starting with `brutal`.
The function should take a single `info` argument, a `dict()`.

This function is called once for each article processed.
It can modify the contents of the `info` dictionary in order to
change the article.
The contents of the `info` dictionary are not very well-defined,
but you can likely rely on:

The article as rendered into HTML text is in `info["body"]`;
this is the most useful thing to alter
(for example, by adding a header or a trailer).
The name of the output file where HTML will be eventually
written is `info["html_name"]`.

The function is called just after the article is converted to HTML,
but before the outer BODY, HEAD, and HTML tags are added.

Any filename starting with `brutal` and ending `.py`
(in the current directory) is compiled and executed.
Any function defined in those files that starts with `brutal`
is called once for each article processed.
Files are processed in ASCIIbetical order.


## Development

Install `pip3` and use

    pip3 install --user --editable .

## END
