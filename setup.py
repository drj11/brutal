#!/usr/bin/env python3

from setuptools import setup

setup(
    name="Brutal",
    version="0.0.0",
    description="Brutal Blogging Software",
    author="David Jones",
    author_email="drj@pobox.com",
    scripts=["brutal"],
    install_requires=["Markdown >=2.6,<3.0", "html5lib>=0.9999999,<1.0"],
)
